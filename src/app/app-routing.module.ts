import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {OrderExistsGuard, OrdersGuard} from './guards';
import {OrderslistComponent} from './containers/orderslist/orderslist.component';
import {OrderComponent} from './containers/order/order.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [OrdersGuard],
    component: OrderslistComponent
  },
  {
    path: 'order/:orderId',
    canActivate: [OrderExistsGuard],
    component: OrderComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
