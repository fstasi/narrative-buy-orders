import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';

import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { tap, map, filter, take, switchMap } from 'rxjs/operators';

import * as fromStore from '../store';
import {IOrder} from '../store/models';

@Injectable()
export class OrderExistsGuard implements CanActivate {

  constructor(private store: Store<fromStore.IApplicationState>) { }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.checkStore().pipe(
      switchMap(() => {
        const orderId = route.params.orderId;
        return this.hasOrder(orderId);
      })
    );
  }

  hasOrder(orderId: string): Observable<boolean> {
    return this.store.select(fromStore.getOrdersEntities).pipe(
      map((entities: { [key: string]: IOrder }) => !!entities[orderId]),
      take(1)
    );
  }

  checkStore(): Observable<boolean> {
    return this.store.select(fromStore.getOrdersLoaded)
      .pipe(
        tap(loaded => {
          console.log(1);
          if (!loaded) {
            console.log(2);
            this.store.dispatch(new fromStore.orderActions.LoadEntities());
          }
        }),
        filter(loaded => loaded),
        take(1)
      );
  }

}
