import { OrdersGuard } from './orders.guard';
import { OrderExistsGuard } from './order-exists.guard';

export const guards: any[] = [OrdersGuard, OrderExistsGuard];

export * from './orders.guard';
export * from './order-exists.guard';
