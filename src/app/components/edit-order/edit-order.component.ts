import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {data_package_types, IOrder} from '../../store/models';
import {BsModalRef} from 'ngx-bootstrap';

@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditOrderComponent implements OnInit {

  @Input() order: IOrder;
  @Input() confirmOrderEvent;

  public data_pkg_types: {
    id: number;
    name: string;
  }[];

  constructor(public modalRef: BsModalRef) {

    this.data_pkg_types = [];
    for (const n in data_package_types) {
      if (typeof data_package_types[n] === 'number') {
        this.data_pkg_types.push({id: <any>data_package_types[n], name: n});
      }
    }

  }

  ngOnInit() {
  }

  confirmOrder(newOrder) {
    this.confirmOrderEvent({...this.order, ...newOrder});
    this.modalRef.hide();
  }

  numberOnly(e) {
    let input;
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if (e.which === 32) {
      return false;
    }
    if (e.which === 0) {
      return true;
    }
    if (e.which < 33) {
      return true;
    }
    input = String.fromCharCode(e.which);
    return !!/[\d\s]/.test(input);

  }

}
