import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {IOrder} from '../../store/models';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {Store} from '@ngrx/store';

import * as fromStore from '../../store';
import {EditOrderComponent} from '../../components/edit-order/edit-order.component';

@Component({
  selector: 'app-edit-order-btn',
  templateUrl: './edit-order-btn.component.html',
  styleUrls: ['./edit-order-btn.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditOrderBtnComponent implements OnInit {

  @Input() order: IOrder;

  modalRef: BsModalRef;

  constructor(
    private store: Store<fromStore.IApplicationState>,
    private modalService: BsModalService) { }

  ngOnInit() {
  }

  dispatchEditOrder(order: IOrder) {
    this.store.dispatch(new fromStore.orderActions.UpsertEntities([order]));
  }

  openEditModal(order: Partial<IOrder> = {}) {
    const initialState = {
      order,
      confirmOrderEvent: (confirmOrder) => this.dispatchEditOrder(confirmOrder)
    };

    this.modalRef = this.modalService.show(EditOrderComponent, {initialState, ignoreBackdropClick: true});
    this.modalRef.content.closeBtnName = 'Close';
  }

}
