import { EntityState } from '@ngrx/entity';

export interface IOrderState extends EntityState<IOrder> {
  entitiesLoaded: boolean;
  entitiesLoading: boolean;
}

export enum data_package_types {
  DeviceLocation = 0,
  DeviceBehavior = 1,
  IDMapping = 2
}

export interface IOrder {
  _id?: string;
  name: string;
  max_bid: number;
  data_pkg_type: data_package_types;
  created: string;
  updated: string;
}
