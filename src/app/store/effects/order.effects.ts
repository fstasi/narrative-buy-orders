import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';

import {catchError, map, switchMap} from 'rxjs/operators';

import {orderActions} from '../actions';
import {BackendConnector} from '../../services/backend-connector.service';
import {of} from 'rxjs/internal/observable/of';

@Injectable()
export class OrdersEffects {
  constructor(private actions$: Actions, private backend: BackendConnector) {}

  @Effect()
  loadOrders$ = this.actions$
    .pipe(
      ofType(orderActions.LOAD_ENTITIES),
      switchMap(() => {
        return this.backend.getOrders().pipe(
          map(entities => {
            return new orderActions.LoadEntitiesSuccess(entities);
          }),
          catchError(error => of(new orderActions.LoadEntitiesFail(error)))
        );
      })
    );

  @Effect()
  upsertOrders$ = this.actions$
    .pipe(
      ofType(orderActions.UPSERT_ENTITIES),
      map((action: orderActions.UpsertEntities) => action.payload),
      switchMap(payload => {
        return this.backend.upsertOrders(payload).pipe(
          map(entities => {
            return new orderActions.UpsertEntitiesSuccess(entities);
          }),
          catchError(error => of(new orderActions.UpsertEntitiesFail(error)))
        );
      })
    );

  @Effect()
  deleteOrder$ = this.actions$
    .pipe(
      ofType(orderActions.REMOVE_ENTITY),
      map((action: orderActions.RemoveEntity) => action.payload),
      switchMap(payload => {
        return this.backend.deleteOrder(payload.orderId).pipe(
          map(deletedId => {
            return new orderActions.RemoveEntitySuccess({orderId: deletedId});
          }),
          catchError(error => of(new orderActions.RemoveEntityFail(error)))
        );
      })
    );

}
