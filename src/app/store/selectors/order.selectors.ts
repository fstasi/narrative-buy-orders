import { createSelector } from '@ngrx/store';

import * as fromFeature from '../reducers/';
import * as fromOrder from '../reducers/order.reducer';

import * as fromModels from '../models';
import { Params } from '@angular/router';
import {getRouterParams} from '../reducers/router.reducer';


export const getOrdersLoading = createSelector(
  fromFeature.getOrdersState,
  fromOrder.getOrdersLoading
);

export const getOrdersLoaded = createSelector(
  fromFeature.getOrdersState,
  fromOrder.getOrdersLoaded
);

// return all orders
export const getOrdersArray = createSelector(
  fromFeature.getOrdersState,
  fromOrder.getArray
);

// return all orders
export const getOrdersEntities = createSelector(
  fromFeature.getOrdersState,
  fromOrder.getEntities
);

export const getCurrentOrder = createSelector(
  getOrdersEntities,
  getRouterParams,
  (orders, routerParams: Params): fromModels.IOrder => {

    if (routerParams && routerParams.orderId && orders) {
      return orders[routerParams.orderId];
    }

    return null;
  }
);

