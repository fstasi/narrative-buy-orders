import { Action } from '@ngrx/store';

import { IOrder } from '../models';

const PREFIX = '[Buy Orders]';

export const LOAD_ENTITIES = `${PREFIX} Load Entities`;
export const LOAD_ENTITIES_SUCCESS = `${PREFIX} Load Entities Success`;
export const LOAD_ENTITIES_FAIL = `${PREFIX} Load Entities Fail`;

export const UPSERT_ENTITIES = `${PREFIX} Upsert Entities`;
export const UPSERT_ENTITIES_SUCCESS = `${PREFIX} Upsert Entities Success`;
export const UPSERT_ENTITIES_FAIL = `${PREFIX} Upsert Entities Fail`;

export const REMOVE_ENTITY = `${PREFIX} Remove Entity`;
export const REMOVE_ENTITY_SUCCESS = `${PREFIX} Remove Entity Success`;
export const REMOVE_ENTITY_FAIL = `${PREFIX} Remove Entity Fail`;

export const CLEAR_ENTITIES = `${PREFIX} Clear Entities`;
export const CLEAR_ENTITIES_SUCCESS = `${PREFIX} Clear Entities Success`;
export const CLEAR_ENTITIES_FAIL = `${PREFIX} Clear Entities Fail`;

export class LoadEntities implements Action {
  readonly type = LOAD_ENTITIES;
  constructor() { }
}
export class LoadEntitiesSuccess implements Action {
  readonly type = LOAD_ENTITIES_SUCCESS;
  constructor(public payload: IOrder[]) { }
}
export class LoadEntitiesFail implements Action {
  readonly type = LOAD_ENTITIES_FAIL;
  constructor(public payload: any) { }
}

export class UpsertEntities implements Action {
  readonly type = UPSERT_ENTITIES;
  constructor(public payload: IOrder[]) { }
}
export class UpsertEntitiesSuccess implements Action {
  readonly type = UPSERT_ENTITIES_SUCCESS;
  constructor(public payload: IOrder[]) { }
}
export class UpsertEntitiesFail implements Action {
  readonly type = UPSERT_ENTITIES_FAIL;
  constructor(public payload: any) { }
}

export class RemoveEntity implements Action {
  readonly type = REMOVE_ENTITY;
  constructor(public payload: {
    orderId: string;
  }) { }
}
export class RemoveEntitySuccess implements Action {
  readonly type = REMOVE_ENTITY_SUCCESS;
  constructor(public payload: {
    orderId: string;
  }) { }
}
export class RemoveEntityFail implements Action {
  readonly type = REMOVE_ENTITY_FAIL;
  constructor(public payload: any) { }
}

export class ClearEntities implements Action {
  readonly type = CLEAR_ENTITIES;
}
export class ClearEntitiesSuccess implements Action {
  readonly type = CLEAR_ENTITIES_SUCCESS;
}
export class ClearEntitiesFail implements Action {
  readonly type = CLEAR_ENTITIES_FAIL;
  constructor(public payload: any) { }
}

export type OrderActions =
  LoadEntities
  | LoadEntitiesSuccess
  | LoadEntitiesFail

  | UpsertEntities
  | UpsertEntitiesSuccess
  | UpsertEntitiesFail

  | RemoveEntity
  | RemoveEntitySuccess
  | RemoveEntityFail

  | ClearEntities
  | ClearEntitiesSuccess
  | ClearEntitiesFail
  ;
