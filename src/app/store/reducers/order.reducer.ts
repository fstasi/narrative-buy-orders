import {createEntityAdapter} from '@ngrx/entity';

import {IOrder, IOrderState} from '../models';
import {orderActions} from '../actions';

export type Action = orderActions.OrderActions;

export const adapter = createEntityAdapter<IOrder>({
  selectId: (order: IOrder) => order._id
});

/// Default app state
const defaultState: IOrderState = adapter.getInitialState({
  // additional entity state properties
  entitiesLoaded: false,
  entitiesLoading: false
});

export function reducer(
  state: IOrderState = defaultState,
  action: Action
): IOrderState {
  switch (action.type) {

    case orderActions.LOAD_ENTITIES: {
      return {...state, entitiesLoading: true};
    }
    case orderActions.LOAD_ENTITIES_SUCCESS: {
      return adapter.addAll(
        (action as orderActions.LoadEntitiesSuccess).payload,
        {...state, entitiesLoading: false, entitiesLoaded: true}
      );
    }
    case orderActions.LOAD_ENTITIES_FAIL: {
      return adapter.addAll([], {
        ...state,
        entitiesLoading: false,
        entitiesLoaded: false
      });
    }

    case orderActions.UPSERT_ENTITIES: {
      return {...state, entitiesLoading: true};
    }
    case orderActions.UPSERT_ENTITIES_SUCCESS: {
      return adapter.upsertMany(
        (action as orderActions.UpsertEntitiesSuccess).payload,
        {...state, entitiesLoading: false}
      );
    }
    case orderActions.UPSERT_ENTITIES_FAIL: {
      return {...state, entitiesLoading: false};
    }

    case orderActions.REMOVE_ENTITY: {
      return {...state, entitiesLoading: true};
    }
    case orderActions.REMOVE_ENTITY_SUCCESS: {
      return adapter.removeOne(
        (action as orderActions.RemoveEntitySuccess).payload.orderId,
        {...state, entitiesLoading: false}
      );
    }
    case orderActions.REMOVE_ENTITY_FAIL: {
      return {...state, entitiesLoading: false};
    }

    case orderActions.CLEAR_ENTITIES: {
      return {...state, entitiesLoading: true};
    }
    case orderActions.CLEAR_ENTITIES_SUCCESS: {
      return adapter.removeAll({...state, entitiesLoading: false});
    }
    case orderActions.CLEAR_ENTITIES_FAIL: {
      return {...state, entitiesLoading: false};
    }

    default: {
      return state;
    }
  }
}

// get the selectors
const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = adapter.getSelectors();

// select the array of user ids
export const getIds = selectIds;

// select the dictionary of user entities
export const getEntities = selectEntities;

// select the array of users
export const getArray = selectAll;

// select the total user count
export const getCount = selectTotal;

export const getOrdersLoaded = (state: IOrderState) =>
  state.entitiesLoaded;
export const getOrdersLoading = (state: IOrderState) =>
  state.entitiesLoading;
