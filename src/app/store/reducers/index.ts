import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';
import {routerReducer, RouterReducerState} from '@ngrx/router-store';

// contains all the reducers
import * as fromRouter from './router.reducer';
import * as fromOrder from './order.reducer';
import * as fromModels from '../models';

export interface IApplicationState {
  router: RouterReducerState<fromRouter.RouterStateUrl>;
  orders: fromModels.IOrderState;
}

export const reducers: ActionReducerMap<IApplicationState> = {
  orders: fromOrder.reducer,
  router: routerReducer
};

export const getOrdersState = createFeatureSelector<fromModels.IOrderState>('orders');
