import { ActivatedRouteSnapshot, RouterStateSnapshot, Params } from '@angular/router';
import { ActionReducerMap, createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';

export interface RouterStateUrl {
  url: string;
  queryParams: Params;
  params: Params;
}

export interface RouterState {
  routerReducer: fromRouter.RouterReducerState<RouterStateUrl>;
}

export const reducers: ActionReducerMap<RouterState> = {
  routerReducer: fromRouter.routerReducer,
};

export const getRouterFeature = createFeatureSelector<
  fromRouter.RouterReducerState<RouterStateUrl>
>('router');

export const getRouterState = createSelector(
  getRouterFeature,
  (router) => router && router.state
);

export const getRouterUrl = createSelector(
  getRouterState,
  (routerState) => {
    console.log('routerState', routerState);
    return routerState && routerState.url;
  }
);

export const getRouterParams = createSelector(
  getRouterState,
  (routerState) => routerState && routerState.params
);

/* serialize all info of the router */
export class CustomSerializer
  implements fromRouter.RouterStateSerializer<RouterStateUrl> {

  serialize(routerState: RouterStateSnapshot): RouterStateUrl {

    const { url } = routerState;
    const { queryParams } = routerState.root;

    let state: ActivatedRouteSnapshot = routerState.root;

    while (state.firstChild) {
      state = state.firstChild;
    }
    const { params } = state;

    return { url, queryParams, params };
  }
}
