import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import { IOrder } from '../store/models/';
import {Observable} from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class BackendConnector {
  private apiEndPoint = '/reporting/';
  constructor(private http: HttpClient) { }

  getOrders(): Observable<IOrder[]> {
    return this.http.get<any>('/orders').pipe(
      catchError(err => this.handleError(err))
    );
  }

  getOrder(orderId: string): Observable<IOrder | null> {
    return this.http.get<any>(`/order/${orderId}`).pipe(
      catchError(err => this.handleError(err))
    );
  }

  upsertOrders(orders: Partial<IOrder>[]): Observable<IOrder[]> {
    return this.http.post<any>('/orders', orders).pipe(
      catchError(err => this.handleError(err))
    );
  }

  deleteOrder(orderId: string): Observable<string> {
    return this.http.delete<any>(`/order/${orderId}`).pipe(
      catchError(err => this.handleError(err))
    );
  }

  private handleError(error) {
    return Observable.throw(error.json());
  }

}
