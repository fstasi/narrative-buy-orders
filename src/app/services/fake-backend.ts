import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import {IOrder} from '../store/models';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // array in local storage for orders
    let orders: IOrder[] = JSON.parse(localStorage.getItem('orders')) || [];

    // wrap in delayed observable to simulate server api call
    return of(null).pipe(mergeMap(() => {

      if (request.url.endsWith('/orders') && request.method === 'POST') {

        const newOrders = JSON.parse(JSON.stringify(request.body)) as IOrder[]; // deep copy to modify the items as a server would do

        newOrders.forEach(newOrder => {
          if (!newOrder._id) {
            newOrder._id = Date.now().toString();
            newOrder.created = new Date().toISOString();
            newOrder.updated = new Date().toISOString();
            orders.push(newOrder);
            return; // move to the next element in the array
          }

          let found = false;
          for (const o in orders) {

            if (orders[o]._id === newOrder._id) {
              found = true;
              newOrder.updated = new Date().toISOString();
              orders[o] = newOrder;
              break;
            }
          }

        });

        localStorage.setItem('orders', JSON.stringify(orders))

        return of(new HttpResponse({ status: 200, body: orders }));

      }

      // get orders
      if (request.url.endsWith('/orders') && request.method === 'GET') {
        return of(new HttpResponse({ status: 200, body: orders }));
      }

      // get order by id
      if (request.url.match(/\/order\/\d+$/) && request.method === 'GET') {

        const urlParts = request.url.split('/');
        const id = urlParts[urlParts.length - 1];
        const matchedOrder = orders.find(order => order._id === id) || null;
        return of(new HttpResponse({ status: 200, body: matchedOrder }));
      }

      // delete order
      if (request.url.match(/\/order\/\d+$/) && request.method === 'DELETE') {
        const urlParts = request.url.split('/');
        const id = urlParts[urlParts.length - 1];
        orders = orders.filter(order => order._id !== id);

        localStorage.setItem('orders', JSON.stringify(orders));

        return of(new HttpResponse({ status: 200, body: id }));
      }

      // pass through any requests not handled above
      return next.handle(request);

    }))
    // call materialize and dematerialize to ensure delay
    .pipe(materialize())
    .pipe(delay(300))
    .pipe(dematerialize());
  }
}

export let fakeBackendProvider = {
  // use fake backend in place of Http service for backend-less development
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};
