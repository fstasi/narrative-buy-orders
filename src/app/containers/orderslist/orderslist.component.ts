import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import {Observable} from 'rxjs';

import * as fromStore from '../../store';

import {IOrder} from '../../store/models';

@Component({
  selector: 'app-orderslist',
  templateUrl: './orderslist.component.html',
  styleUrls: ['./orderslist.component.scss']
})
export class OrderslistComponent implements OnInit {

  orders$: Observable<IOrder[]>;

  constructor(
    private store: Store<fromStore.IApplicationState>,
  ) { }

  ngOnInit() {
    this.orders$ = this.store.select(fromStore.getOrdersArray);
  }

  dispatchRemoveOrder(orderId: string) {
    this.store.dispatch(new fromStore.orderActions.RemoveEntity({orderId}));
  }

}
