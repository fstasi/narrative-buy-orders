import { Component, OnInit, OnDestroy } from '@angular/core';
import {Params} from '@angular/router';

import { Store } from '@ngrx/store';

import {Subscription} from 'rxjs';
import * as fromStore from '../../store';
import {getRouterUrl} from '../../store/reducers/router.reducer';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit, OnDestroy {

  private routerUrlSubscription$: Subscription;
  public urlArray: {
    text: string,
    link: string
  }[];

  constructor(private store: Store<fromStore.IApplicationState>) { }

  ngOnInit() {
    this.routerUrlSubscription$ = this.store.select(getRouterUrl).subscribe(url => {

      if (!url) { return; } // handle undefined on store init

      const urlArray = url.split('/').filter(e => e.length > 0);

      if (urlArray.length === 0) {
        this.urlArray = [{
          text: 'Orders',
          link: ''
        }];
      } else {
        this.urlArray = urlArray.map((str, i) => {

          if (i === 0) {
            return {
              text: 'Orders',
              link: '/'
            };
          }

          return {
            text: str,
            link: ''
          };


        });
      }


    });
  }

  ngOnDestroy() {
    // unsubscribe on component destroy
    this.routerUrlSubscription$.unsubscribe();
  }


}
