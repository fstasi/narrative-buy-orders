import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromStore from '../../store';

import {data_package_types, IOrder} from '../../store/models';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  order$: Observable<IOrder>;
  constructor(private store: Store<fromStore.IApplicationState>) { }

  ngOnInit() {
    this.order$ = this.store.select(fromStore.getCurrentOrder);
  }

  resolvePackageType(type: data_package_types) {
    return data_package_types[type];
  }

}
