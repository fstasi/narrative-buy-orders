import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { environment } from '../environments/environment';


// NGRX
import {MetaReducer, StoreModule} from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {RouterStateSerializer, StoreRouterConnectingModule} from '@ngrx/router-store';
import {CustomSerializer} from './store/reducers/router.reducer';
import { effects } from './store/effects';
import { reducers } from './store/reducers';
// not used in production
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { storeFreeze } from 'ngrx-store-freeze';
export const metaReducers: MetaReducer<any>[] = !environment.production ? [storeFreeze] : [];

// GUARDS
import * as fromGuards from './guards';

// BOOTSTRAP COMPONENTS
import {
  ButtonsModule, CollapseModule,
  ModalModule
} from 'ngx-bootstrap';

// SERVICES
import { BackendConnector } from './services/backend-connector.service';
// used to create fake backend
import { fakeBackendProvider } from './services/fake-backend';

// ROUTING
import { AppRoutingModule } from './app-routing.module';

// COMPONENTS
import { AppComponent } from './app.component';
import { OrderslistComponent } from './containers/orderslist/orderslist.component';
import { OrderComponent } from './containers/order/order.component';
import { EditOrderComponent } from './components/edit-order/edit-order.component';
import { BreadcrumbComponent } from './containers/breadcrumb/breadcrumb.component';
import { EditOrderBtnComponent } from './components/edit-order-btn/edit-order-btn.component';

@NgModule({
  declarations: [
    AppComponent,
    OrderslistComponent,
    OrderComponent,
    EditOrderComponent,
    BreadcrumbComponent,
    EditOrderBtnComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,

    // ROUTING
    AppRoutingModule,

    // NGRX
    StoreModule.forRoot(reducers, {metaReducers}),
    StoreRouterConnectingModule.forRoot(),
    EffectsModule.forRoot(effects),
    StoreDevtoolsModule.instrument({maxAge: 25, logOnly: environment.production}),

    // BOOTSTRAP MODULES
    ButtonsModule.forRoot(),
    ModalModule.forRoot(),
    CollapseModule,
  ],
  providers: [
    ...fromGuards.guards,
    { provide: RouterStateSerializer, useClass: CustomSerializer },
    BackendConnector,
    // provider used to create fake backend
    fakeBackendProvider
  ],
  entryComponents: [EditOrderComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
