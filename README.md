# BuyOrders

This is a simple project based on Angular 7+. App State management is handled by NgRx.

### NgRx state management
The choice to use a Redux-Like store for such a simple application may look like an overkill,
but it provides at the same time:
- scalability - it will be easier to manage the state when the app will grow
- robustness - state changes are predictable and debuggable
- performance (i) - items in the store are immutables, this allow us to set OnPush and avoid the CPU cost of the default change detection 
- performance (ii) - state selector make use of memoization: a sort of cache used to re-calculate the output of a function only when the input changes

### SmartComponents / Dumb Components
The Smart-Components (containers) / Dumb Components Pattern is a great choice to make the code reusable.

The Smart-Components are responsible of store subscription (and dispatch of new actions). Typically they are those components conencted to the routes.
Dumb components, on the other hand, have no clue about the store, and they are given the
needed objects by a container components or by other dumb component.

This way is easy to move our Dumb components around, and use them in different routes without worry about the current state.
You can have a look edit-order.component as a good example of how dumb components re-usable and state-transparent;

### Store-friendly Route Guards
Guards are used to pre-load the state on route-loading. This ensures the state is loaded when the component is loaded. 

### Fake Backend
As a proof of concept, this application make use of LocalStorage to provide persistency to data. But, thanks to HttpInterceptors,
make the api calls reach a server will be as easy as removing the fake-backend service.

The interceptor is used to catch the request to a particular URL, save to the local storage, and provide a valid response.

To make everything feel more real it also adds a 300 millisecs latency!

### UI and CSS
Bootstrap is used to provide mobile compatibility and a basic look and feel.

# Dependencies
- nodejs v8+
- [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8. (`npm install -g @angular/cli`)

# Install Instructions
- cd into project root directory
- install dependencies via `npm i`
- run locally on you computer `ng serve`
- visit http://localhost:4200/



## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
